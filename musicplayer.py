import tkinter as tk
from pygame import mixer
from tkinter import filedialog

mixer.init()

root = tk.Tk()
root.title('Music Player')
songs = tk.Tk()
songs.title('Songs')
s = []
FONT = ('Arial', 16)


class song:
    def __init__(self, name):
        self.name = name

        def play_mus(name):
            mixer.music.load(name)
            mixer.music.play()
        self.btn = tk.Button(songs, text='Play "' + self.name + '"', command=lambda: play_mus(self.name), padx=50, pady=20)
        self.btn.pack()


def add_songs():
    name = list(filedialog.askopenfilenames(title='Select Audio File', filetypes=(('MP3 Audio Format', '*.mp3'), ('Wavlength Audio Format', '*.wav'))))
    for i in range(len(name)):
        length = len(s)
        s.append('')
        s[length] = song(name[i])


def play_sound(*args):
    arg = list(args)
    if len(arg) < 1:
        dirr = filedialog.askopenfilename(title='Select Audio File', filetypes=(('MP3 Audio Format', '*.mp3'), ('Wavelength Audio Format', '*.wav')))
        mixer.music.load(dirr)
        mixer.music.play()
        length = len(s)
        s.append('')
        s[length] = song(dirr)

    if len(arg) != 0:
        mixer.music.load(arg)
        mixer.music.play()


def stop_sound():
    mixer.music.load('recording1.wav')
    mixer.music.play()


btn = tk.Button(root, text='Select File', command=play_sound, padx=50, pady=25, font=FONT)
btn.grid(row=0, column=0, padx=25)

placeholder = tk.Label(root, text='           ')
placeholder.grid(row=1, column=0)

btn_stop = tk.Button(root, text='Stop Mucic', command=stop_sound, padx=50, font=FONT)
btn_stop.grid(row=2, column=0, padx=25)

placeholder = tk.Label(root, text='           ')
placeholder.grid(row=3, column=0)

songs_btn = tk.Button(root, text='Add Songs', command=add_songs, padx=50, font=FONT)
songs_btn.grid(row=6, column=0, padx=25)
root.mainloop()